% a script to make a movie noise cancelling headphones post
clear all

nx = 600;
ny = 400;

incoming_noise = zeros(ny,nx,3);
noise_cancelling = zeros(ny,nx,3);
nc_speaker = zeros(ny,nx);

%wave speed
c = 1;
%time step
dt = .125;
% spatial step
dx = .5;
%time steps to integrate
time_steps = 1500;
% period of the incoming wave (time steps per cycle)
T = 50;
% Angular frequency
omega = 2*pi/T; % one cycle every

%set zeros for the noise cancelling signal to give it time to start up
microphone = zeros(300,1);
% Effeciency of the noise cancelling system 0<efficiency<1
efficiency = 1;
% is the noise cancelling turned on?
nc_active = 1;

%sponge to represent the source for the nc speaker
for i = 1:ny
    for j = 1:nx
        nc_speaker(i,j) = exp(-((i-ny/2)^2+(j-ceil(nx/4))^2)/50);
    end
end

% predefine some loo variables to stop matlab complaining
ear_signal_incoming = 0;
ear_signal_nc = 0;
ear_signal_net = 0;

%Define the figure and get it all sorted out nicely to speed up the loop
h = figure(19);%('visible', 'off');
clf
subplot(4,2,1)
h1 = surf(incoming_noise(:,:,3));
set(gcf(), 'Renderer', 'zbuffer')
shading flat
caxis([-2,2])
title('Incoming noise','FontSize',16)
set(gca, 'DataAspectRatio', [diff(get(gca, 'XLim')) diff(get(gca, 'XLim')) diff(get(gca, 'ZLim'))])
view(0,90)
set(h1,'ZDataSource','incoming_noise(:,:,3)');

subplot(4,2,2)
h2 = surf(noise_cancelling(:,:,3));
set(gcf(), 'Renderer', 'zbuffer')
shading flat

caxis([-2,2])
title('Active noise cancelling signal','FontSize',16)
set(gca, 'DataAspectRatio', [diff(get(gca, 'XLim')) diff(get(gca, 'XLim')) diff(get(gca, 'ZLim'))])
view(0,90)
set(h2,'ZDataSource','noise_cancelling(:,:,3)');

subplot(4,2,[3,4,5,6])
h3 = surf(incoming_noise(:,:,3)+noise_cancelling(:,:,3));
set(gcf(), 'Renderer', 'zbuffer')
shading flat
colorbar('EastOutside')
caxis([-2,2])
titlestring = {'Combination of incoming noise and noise cancelling  ',...
    'Black dot is location of the listener''s ear  '};
title(titlestring,'FontSize',16)
hold on
plot3(nx/3,ny/2,1.1,'.k','MarkerSize',20)
view(0,90)
hold off
set(gca, 'DataAspectRatio', [diff(get(gca, 'XLim')) diff(get(gca, 'XLim')) diff(get(gca, 'ZLim'))])
set(h3,'ZDataSource','incoming_noise(:,:,3)+noise_cancelling(:,:,3)');

subplot(4,2,[7,8])
h4 = plot(ear_signal_incoming,'LineWidth',3);
set(h4,'YDataSource','ear_signal_incoming');
legenddata{1} = 'Incoming signal   ';
hold on
h5 = plot(ear_signal_nc,'r','LineWidth',3);
set(h5,'YDataSource','ear_signal_nc');
legenddata{2} = 'Noise cancelling signal   ';
h6 = plot(ear_signal_net,'k','LineWidth',3);
set(h6,'YDataSource','ear_signal_net');
legenddata{3} = 'Summation of both  i ';
hold off
xlim([600,time_steps])
ylim([-1.1,1.1])
xlabel('Time','FontSize',16)
ylabel('Signal strength','FontSize',16)
titlestring = {'Incoming noise (blue), active noise cancelling (red)   ',...
    'and their combination (black)   '};
title(titlestring,'FontSize',16)
%legend(legenddata,'Location','SouthOutside','FontSize',16)
counter = 0;


for n = 1:time_steps
    for i = 2:ny-1
        for j = 2:nx-1
            incoming_noise(i,j,3) = 2*incoming_noise(i,j,2)-incoming_noise(i,j,1)+((incoming_noise(i+1,j,2)-2*incoming_noise(i,j,2)+...
                incoming_noise(i-1,j,2))+(incoming_noise(i,j+1,2)-2*incoming_noise(i,j,2)+...
                incoming_noise(i,j-1,2))*(c^2*dt^2/(dx^2)));
        end
    end
    
    % Boundary conditions - zero normal derivative
    incoming_noise(1,:,3) = incoming_noise(2,:,3);
    incoming_noise(end,:,3) = incoming_noise(end-1,:,3);
    incoming_noise(:,end,3) = 0;
    
    %Inflow condition
    incoming_noise(:,1,3) = ones(ny,1)*sin(omega*n*dt);
    
    % signal at microphone location
    microphone(n+300) = incoming_noise(ny/2,nx/6,3);
    
    % Signal to play to cancel the incoming noise at the ear
    %time it takes the noise to propagate from the microphone at (ny/2,nx/6) to the
    %speaker (at (ny/2,ceil(nx/4)) in time steps
    time_delay = ((abs(ceil(dx*nx/6) - ceil(dx*nx/4)))/c)/dt;
    
    
    noise_cancelling_signal(n) = -efficiency*microphone(300+n-time_delay);
    
    for i = 2:ny-1
        for j = 2:nx-1
            noise_cancelling(i,j,3) = 2*noise_cancelling(i,j,2)-...
                noise_cancelling(i,j,1)+((noise_cancelling(i+1,j,2)-...
                2*noise_cancelling(i,j,2)+...
                noise_cancelling(i-1,j,2))+(noise_cancelling(i,j+1,2)-...
                2*noise_cancelling(i,j,2)+...
                noise_cancelling(i,j-1,2)))*(c^2*dt^2/(dx^2));
        end
    end
    
    % Introduce the noise cancelling signal
    for i =ny/2-3:ny/2+3
        for j = ceil(nx/4)-3:ceil(nx/4)+3
            noise_cancelling(i,j,3) = 3*noise_cancelling_signal(n)*nc_speaker(i,j);
        end
    end
    
    % Boundary conditions - zero normal derivative
    noise_cancelling(1,:,3) = noise_cancelling(2,:,3);
    noise_cancelling(end,:,3) = noise_cancelling(end-1,:,3);
    noise_cancelling(:,end,3) = 0;
    noise_cancelling(:,1,3) = 0;
    
    % what is received by the ear at at (ny/2,ceil(3*nx/8))
    ear_signal_net(n) = noise_cancelling(ny/2,ceil(nx/3),3)+incoming_noise(ny/2,ceil(nx/3),3);
    ear_signal_nc(n) = noise_cancelling(ny/2,ceil(nx/3),3);
    ear_signal_incoming(n) = incoming_noise(ny/2,ceil(nx/3),3);
    
    
    if mod(n,5)==1
        counter = counter+1;
    % refressh the plot
    refreshdata
    picture_name = sprintf('picture%04u',counter);
    
    img = getframe(gcf);
    imwrite(img.cdata, [picture_name, '.png']);
    end
    
    % move levels down ready for next time step
    incoming_noise(:,:,1) = incoming_noise(:,:,2);
    incoming_noise(:,:,2) = incoming_noise(:,:,3);
    noise_cancelling(:,:,1) = noise_cancelling(:,:,2);
    noise_cancelling(:,:,2) = noise_cancelling(:,:,3);
    
end