
A MATLAB script that explores the basic concept behind noise cancelling headphones. 

An incoming wave is generated and sampled. From this an active noise cancelling signal is created that cancels much of the incoming noise field at the location of the listener's ear.

This code was written for a blog post I wrote about my new noise cancelling
headphones. The post can be found here: http://doddridge.me/2014/04/16/noise-cancelling-headphones/
